from django.contrib import admin
from .models import *

admin.site.register(User)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(Leaderboard)
admin.site.register(FichaCompletacion)
admin.site.register(Niveles)
admin.site.register(ProfileAdministration)


