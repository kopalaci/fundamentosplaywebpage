from rest_framework import serializers

from .models import FichaCompletacion ,Leaderboard


class FichaSerializer(serializers.ModelSerializer):

    class Meta:

        model = FichaCompletacion

        fields = ('nivel','principianteAcertadas','principianteFalladas','intermedioFalladas','intermedioAcertadas'
                  ,'expertoFalladas','expertoAcertadas')


class LeaderBoardSerializer(serializers.ModelSerializer):

    class Meta:
        model = Leaderboard
        fields = '__all__'