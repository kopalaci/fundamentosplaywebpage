import datetime
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.mail import send_mail
from django.db import models
from django.utils import timezone
from django.utils.html import escape, mark_safe


class AbstractUser(AbstractBaseUser, PermissionsMixin):

    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        ('Usuario'),
        max_length=150,
        unique=True,
  #      help_text=('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': ("El nombre de usuario ya se encuentra registrado."),
        },
    )
    first_name = models.CharField(('Nombres'), max_length=30, blank=True, db_column="nombre")
    last_name = models.CharField(('Apellidos'), max_length=150, blank=True, db_column="apellido")
    email = models.EmailField(('Correo'), blank=True)
    is_staff = models.BooleanField(
        ('staff status'),
        default=False,
        help_text=('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        ('active'),
        default=True,
        help_text=(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(('date joined'), default=timezone.now)

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = ('user')
        verbose_name_plural = ('users')
        abstract = True

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)

class User(AbstractUser):
    is_student = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    userHash = models.CharField(max_length=150, unique=True ,null=True , db_column="userHash" )
    is_assistant = models.BooleanField(default=False)
    phone = models.CharField(max_length=30, null=True,blank=True)
    maximo_nivel= models.IntegerField(default=1, auto_created=True , db_column="maximo_nivel")
    ultimonivel= models.IntegerField(default=1, auto_created=True , db_column="ultimo_nivel_visitado")
    preguntas_totales = models.IntegerField( default=0, auto_created=True)
    preguntas_correctas = models.IntegerField( default=0, auto_created=True)
    preguntas_erroneas = models.IntegerField( default=0, auto_created=True)
    avatar= models.IntegerField( default=-1, auto_created=True)
    is_logged = models.BooleanField(default=False, auto_created=True)

    profesor = models.ForeignKey('self', on_delete=models.CASCADE, null=True, related_name="teacher" ,blank=True)
    def __str__(self):
        return self.username

    class Meta:
        db_table = 'usuarios'


class Niveles(models.Model):
    nivel = models.IntegerField(primary_key=True)
    preguntas_totales = models.IntegerField()
    preguntas_acertadas = models.IntegerField()
    preguntas_erroneas = models.IntegerField()

    def __str__(self):
        return str(self.nivel)

    class Meta:
        db_table = 'niveles'


class FichaCompletacion(models.Model):
    fichaid = models.AutoField(db_column='fichaId', primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE , db_column='userId')
    nivel = models.OneToOneField(Niveles, models.DO_NOTHING, db_column='nivel')
    medallaPrincipiante = models.BooleanField(default=False, db_column='medalla_principiante')
    medallaIntermedio = models.BooleanField(default=False , db_column='medalla_intermedio')
    medallaExperto = models.BooleanField(default=False ,db_column= 'medalla_experto')
    principianteFalladas = models.IntegerField(default=0, db_column='principiante_falladas')
    principianteAcertadas = models.IntegerField(default=0, db_column='principiante_acertadas')
    intermedioFalladas = models.IntegerField(default=0, db_column='intermedio_falladas')
    intermedioAcertadas = models.IntegerField(default=0, db_column='intermedio_acertadas')
    expertoFalladas = models.IntegerField(default=0, db_column='experto_falladas')
    expertoAcertadas = models.IntegerField(default=0, db_column='experto_acertadas')

    class Meta:
        db_table = 'ficha_completacion'


class Leaderboard(models.Model):
    userid = models.ForeignKey(User, models.DO_NOTHING, db_column='userId')  # Field name made lowercase.
    tiempo = models.CharField(max_length=30)
    nivel = models.OneToOneField(Niveles, models.DO_NOTHING, db_column='nivel', primary_key=True)
    start_day = models.DateField()

    class Meta:
        db_table = 'leaderboard'
    def getTiempo(self):
        return round(float(self.tiempo)/1000,2)


class Question(models.Model):

    FACIL  = 0
    MEDIO   = 1
    DIFICIL    = 2

    DIFICULTAD_CHOICES = (
        (FACIL, "Fácil"),
        (MEDIO  ,"Medio"),
        (DIFICIL     ,"Difícil"),
    )

    APROBADA      = 'Aprobada'
    BORRADOR      = 'Borrador'
    POR_CORREGIR  = 'Por Corregir'
    POR_APROBAR   = 'Enviada'

    STATE_CHOICES = (
        (APROBADA     ,'Aprobada'),
        (BORRADOR     ,'Borrador'),
        (POR_CORREGIR ,'Por Corregir'),
        (POR_APROBAR  ,'Enviada'),
    )
    
    state = models.CharField(max_length=50,choices=STATE_CHOICES)
    text = models.CharField('Contenido', max_length=255, db_column="contenido")
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='questions')
    level = models.ForeignKey(Niveles, models.DO_NOTHING, db_column='nivel')
    Dificultad = models.IntegerField(choices=DIFICULTAD_CHOICES, db_column='dificultad')
 #   estado = models.ForeignKey(Estado, on_delete=models.CASCADE, related_name='questions', null=True)
    fecha = models.DateTimeField('Fecha', auto_now=True)
    observacion = models.CharField('Observación', max_length=255, null=True , blank=True)
    image = models.CharField('Imagen', max_length=255, null=True, db_column="imagen" ,blank=True)
    tiempo =  models.FloatField('Tiempo' , null=False, db_column='tiempo_en_segundos')

    revisor = models.ForeignKey(User, on_delete=models.CASCADE, related_name='revisor', null=True ,blank=True)
    veces_correcta=models.IntegerField(default=0, auto_created=True, db_column='veces_acertada')
    veces_incorrecta = models.IntegerField(default=0, auto_created=True, db_column='veces_fallada')

    class Meta:
        db_table = 'preguntas'
        ordering = ['-fecha']

    def __str__(self):
        return self.text[:20] + "..."

    def get_html_badge(self):
        nombre = escape(self.state)
        colores = {"APROBADA": "green", "POR APROBAR": "blue", "BORRADOR": "gray", "POR CORREGIR": "red"}
        html = '<h4><span class="badge badge-primary" style="background-color: %s">%s</span></h4>' % (
        colores[nombre.upper()], self.state)
        return mark_safe(html)


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')
    text = models.CharField('Respuesta', max_length=255)
    is_correct = models.BooleanField('Correcta', default=False)
    retroalimentacion = models.CharField(max_length=255,null=True, blank=True )

    def __str__(self):
        return self.text
    class Meta:
        db_table = 'respuestas'





class ProfileAdministration(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    activation_key = models.CharField(max_length=40, blank=True)
    key_expires = models.DateTimeField(default=datetime.date.today())
    is_confirmed = models.BooleanField('Confirmado', default=False)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural=u'Perfiles de Usuario'
        db_table = 'profile_administration'
