from django.conf.urls.static import static
from django.urls import include, path
from django.contrib import admin
from Proyecto import settings
from .views import apiREST
from .views import home,teachers, question, administrator




urlpatterns = [

    path('', home.home, name='home'),
    path('admin/', admin.site.urls),
    path('profile/<str:username>/', home.view_profile, name='profile'),
    path('user/<int:user_pk>/rol/' ,home.edit_rol, name='edit_rol'),
    path('user/<str:user_username>/', home.edit_user_password, name='edit_user_password'),
    path('user/<int:user_pk>/delete/', home.user_delete, name='user_delete'),
    path('user/<int:user_pk>/active/', home.user_active, name='user_active'),
    path('email_confirm/<str:activation_key>/', home.email_confirm, name='email_confirm'),
    path('logout', home.logout, name='salir'),
    path('descarga', home.descarga, name='descarga'),
    path('leaderboard', home.leaderboard, name='leaderboard'),
    path('contactanos', home.sendMail, name='sendMail'),

   #__________________________________ URL PARA API REST _____________________________-
    path('api/', include(([
        #url de servicios
        path('medallas/<str:username>/', apiREST.Ficha.as_view()),
        path('nivel/<str:username>/', apiREST.Nivel.as_view()),
        path('leaderboard/', apiREST.LeaderBoard.as_view(), name='leaderboard'),
    ], 'FundamentosPlay'), namespace='api')),

   #______________________________  URLS PARA  ADMINISTRACIÓN ________________________
        path('questions/', include(([
        path('', question.QuestionListView.as_view(), name='question_change_list'),
        path('question/edit/<int:question_pk>/', question.question_change, name='question_change'),
        #acciones de perguntas crear , guardar , filtrar , borrar
        path('question/add/', question.question_add, name="question_add"),
        path('question/save/<int:question_pk>/', question.question_borrador, name='question_borrador'),
        path('question/save/', question.question_borrador_nueva_pregunta, name='question_borrador_nueva_pregunta'),
        path('question/filter/<str:filter>/', question.question_filter, name='question_filter'),
        path('question/<int:question_pk>/delete/', question.question_delete, name='question_delete'),
    ], 'FundamentosPlay'), namespace='questions')),

    path('teachers/', include(([
        #accion aprobacion/rechazo de preguntas
        path('question/approve/<int:question_pk>/', teachers.question_aprove_view, name='question_approve'),
        path('question/dimiss/<int:question_pk>/', teachers.question_dimiss, name='question_dimiss'),
        #lista de preguntas por aprobar
        path('question/approve/', teachers.QuestionApproveList.as_view(), name="to_approve_list"),
        #lista de estudiantes
        path('mystudents/list', teachers.MyStudentListView.as_view(), name='my_students'),
        #lista de estudiantes por aprobar
        path('students/approve', teachers.StudentToApproveViewList.as_view(), name='students_approve_list'),
        # accion Aprobacion/Rechazo de estudiantes
        path('mystudents/<str:student_username>/approve', teachers.student_approve, name='student_approve'),
        path('mystudents/<str:student_username>/dismiss', teachers.student_dismiss, name='student_dismiss')
    ], 'FundamentosPlay'), namespace='teachers')),


path('administrator/', include(([
        #accion aprobacion/rechazo de preguntas

        path('create_teacher', administrator.create_teacher, name='create_teacher'),
        path('create_assistant', administrator.create_assistant, name='create_assistant'),
        path('users', administrator.UserListView.as_view(), name='user_list'),
        path('questions', administrator.QuestionListView.as_view(), name='question_list'),
        path('question/edit/<int:question_pk>/', administrator.question_change, name='question_change'),
        path('question/<int:question_pk>/delete/', administrator.question_delete, name='question_delete'),





    ], 'FundamentosPlay'), namespace='administrator')),




]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)