import requests
from django.contrib import messages
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

import hashlib

from requests.auth import HTTPBasicAuth
from django.contrib.auth import get_user_model
User = get_user_model()

class AuthBackend:
    def loginRequest(self, username, password):
        hashpassword= hashlib.sha256(password.encode('utf-8')).hexdigest()
        url = "http://200.9.176.59/FundamentosPlay/servicios/usuarios/login/"+username+"/"+hashpassword
        request = requests.get(url, auth=HTTPBasicAuth('admingamma', 'admin'))
        if(request.status_code == requests.codes.ok):
            return request.text
        return None
    def authenticate(self, request, username=None, password=None):
        userHash = self.loginRequest(username,password)
        if userHash != None:
            try:
                user = User.objects.get(userHash=userHash)
            except ObjectDoesNotExist:
                # Create a new user. There's no need to set a password
                # because only the password from settings.py is checked.
                user = User(username=username, userHash = userHash , password = hashlib.sha256(password.encode('utf-8')).hexdigest())
                user.save()
            return user
        messages.error(request, 'Tus credenciales no son correctas o la cuenta no está activa')
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except ObjectDoesNotExist:
            return None