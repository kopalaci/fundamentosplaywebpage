from django.contrib.auth import login
from django.shortcuts import  redirect
from django.views.generic import (CreateView)
from ..forms import  AssistantSignUpForm
from ..models import  User



#Registro de Ayudantes
class AssistantSignUpView(CreateView):
    model = User
    form_class =AssistantSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'Ayudante'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('questions:question_change_list')


