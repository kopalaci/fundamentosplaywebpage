#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import hashlib
import random
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from django.forms import inlineformset_factory
from django import forms
from FundamentosPlay.models import Question, Answer, User, ProfileAdministration
from FundamentosPlay.views.utils import badges_counter, send_email
from Proyecto import settings
from ..decorators import admin_required, has_permission
from ..forms import TeacherSignUpForm, BaseAnswerInlineFormSet, QuestionForm, QuestionAdminForm, AssistantSignUpForm, \
    RolForm
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm, SetPasswordForm

@login_required
@admin_required
def create_teacher(request):
    if request.method == 'POST':
        form =TeacherSignUpForm(request.POST)
        if form.is_valid():
            user =form.save()
            #------------------------- Envio de correo  de confirmacion de cuenta-------
            salt = hashlib.sha1( (str(random.random())).encode('utf-8')).hexdigest()[:5]
            activation_key = hashlib.sha1((salt+user.email).encode('utf-8') ).hexdigest()
            key_expires = datetime.datetime.today() + datetime.timedelta(2)
            profileadministration = ProfileAdministration(user=user, activation_key=activation_key,   key_expires=key_expires)
            profileadministration.save()
            email_subject = 'Confirmación de cuenta'
            email_body = "Hola "+user.username+ ", Bienvenido a Fundamentos Play. Para poder jugar y acceder al sistema da clíck en este link en menos de 48 horas: \n" \
                         "http://"+ request.get_host()+"/email_confirm/"+activation_key
            send_email(user.email, email_subject,  email_body)
            #------------------------------------------------------------------
            messages.success(request,'Profesor creado exitosamente, se ha enviado un email de verificación.')
            return redirect('administrator:user_list')
        else:
            form = TeacherSignUpForm(request.POST)
            return render(request, 'FundamentosPlay/administrator/create_teacher.html', {
        'form': form,

    })

    # accion get para presentacion de formulario de registro estudiantes
    else:
        form = TeacherSignUpForm()
        return render(request, 'FundamentosPlay/administrator/create_teacher.html',
                      {"form": form})


@login_required
@admin_required
def create_assistant(request):
    if request.method == 'POST':
        form =AssistantSignUpForm(request.POST)
        if form.is_valid():
            user=form.save()
             #------------------------- Envio de correo  de confirmacion de cuenta-------
            salt = hashlib.sha1( (str(random.random())).encode('utf-8')).hexdigest()[:5]
            activation_key = hashlib.sha1((salt+user.email).encode('utf-8') ).hexdigest()
            key_expires = datetime.datetime.today() + datetime.timedelta(2)
            profileadministration = ProfileAdministration(user=user, activation_key=activation_key,   key_expires=key_expires)
            profileadministration.save()
            email_subject = 'Confirmación de cuenta'
            email_body = "Hola "+user.username+ ", Bienvenido a Fundamentos Play. Para poder jugar y acceder al sistema da clíck en este link en menos de 48 horas: \n" \
                         "http://"+ request.get_host()+"/email_confirm/"+activation_key
            send_email(user.email, email_subject,  email_body)
            #------------------------------------------------------------------
            messages.success(request,'Ayudante creado exitosamente, se ha enviado un email de verificación.')
            return redirect('administrator:user_list')
        else:
            form = AssistantSignUpForm(request.POST)
            return render(request, 'FundamentosPlay/administrator/create_assistant.html', {
        'form': form,

    })

    else:
        form = AssistantSignUpForm()
        return render(request, 'FundamentosPlay/administrator/create_assistant.html',
                      {"form": form})



@method_decorator([login_required, admin_required], name='dispatch')
class UserListView(ListView):
    context_object_name = 'users'
    template_name = 'FundamentosPlay/administrator/user_list.html'
    def get_queryset(self):
        queryset = User.objects.exclude(pk=self.request.user.pk)
        return queryset

    def get_context_data(self, **kwargs):
        extra_context ={"counts": badges_counter(self.request.user)}
        context = super(self.__class__, self).get_context_data(**kwargs)
        for key, value in extra_context.items():
            if callable(value):
                context[key] = value()
            else:
                context[key] = value
        return context



#Lista de perguntas totales
@method_decorator([login_required, has_permission], name='dispatch')
class QuestionListView(ListView):

    ordering = ('fecha', )
    context_object_name = 'questions'
    template_name = 'FundamentosPlay/administrator/question_list.html'


    def get_queryset(self):
        queryset = Question.objects.exclude(state="Borrador")
        return queryset



    def get_context_data(self, **kwargs):

        extra_context ={"counts": badges_counter(self.request.user)}
        context = super(self.__class__, self).get_context_data(**kwargs)
        for key, value in extra_context.items():
            if callable(value):
                context[key] = value()
            else:
                context[key] = value
        return context





#vista para editar una pregunta
@login_required
@has_permission
def question_change(request,  question_pk):
    question = Question.objects.get(pk=question_pk)
    AnswerFormSet = inlineformset_factory(
        Question,  # parent model
        Answer,  # base model
        fields=('text', 'is_correct', 'retroalimentacion'),
        formset=BaseAnswerInlineFormSet,
        min_num=2,
        validate_min=True,
        max_num=4,
        validate_max=True,
        widgets={
            'text': forms.Textarea(attrs={'cols': 80, 'rows': 5}),
            'retroalimentacion': forms.Textarea(attrs={'cols': 80, 'rows': 5}),
        }
    )
    if request.method == 'POST':
        form = QuestionAdminForm(request.user ,question, request.POST, instance=question)
        formset = AnswerFormSet(request.POST ,instance=question)
        if form.is_valid():
            question = form.save(commit=False)
            question.state = "Aprobada"
            if len(request.FILES) > 0 and request.FILES['Imagen']:
                myfile = request.FILES['Imagen']
                fs = FileSystemStorage()
                filename = fs.save(str(request.user.username)+str(datetime.date.today())+"."+myfile.name.split(".")[1],myfile)
                question.image = "http://"+request.get_host()+getattr(settings, "MEDIA_URL", None)+filename
            if (formset.is_valid()):
                with transaction.atomic():
                    form.save()
                    formset.save()
                send_email(question.revisor.email, "Pregunta Recibida",
                           question.owner.username + " ha aprobado su pregunta")
                messages.success(request, 'Pregunta aprobada exitosamente!')
                return redirect('administrator:question_list')


        else:
            return render(request, 'FundamentosPlay/administrator/question_list.html', {
                'question': question,
                'form': form,
                'formset': formset,
                'counts': badges_counter(request.user)})
    else:
        form = QuestionAdminForm(request.user, question, instance=question)
        formset = AnswerFormSet(instance=question)
    return render(request, 'FundamentosPlay/administrator/question_edit.html', {
        'question': question,
        'form': form,
        'formset': formset
    })



#borrar una pregunta
@login_required
@has_permission
def question_delete(request,  question_pk):
    if (request.method == 'GET'):
        question = Question.objects.get(pk=question_pk)
        question.delete()
        messages.success(request, 'Pregunta Borrada exitosamente')
        return redirect('administrator:question_list')






