import hashlib
import random
import socket
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import SetPasswordForm
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth import (
    update_session_auth_hash,
)
from FundamentosPlay.decorators import has_permission
from FundamentosPlay.forms import StudentSignUpForm, RolForm
from FundamentosPlay.views.utils import badges_counter, getTipo, send_email
from ..models import *
from django.contrib.auth import get_user_model
User = get_user_model()



def home(request):


    if request.user.is_authenticated:
        request.user.is_logged = True
        request.user.save()

        if(request.user.is_admin):
            return redirect('administrator:user_list')
        profileadministration = ProfileAdministration.objects.get(user=request.user)
        if(profileadministration.is_confirmed):
            #si es profesor o ayudante
            if request.user.is_teacher or request.user.is_assistant:
                return redirect('questions:question_change_list')
            #si es estudiante
            if request.user.is_student :
                #y ha sido aprobado
                if request.user.is_active:
                    return redirect('profile', request.user.username)
                else:
                    messages.info(request,"Tu profesor aún no ha aprobado la cuenta")
                    return redirect('login')

        else:
            messages.info(request,"Aún no has confirmado tu email")
            return redirect('login')

    return render(request, 'FundamentosPlay/home.html',{'leaders':Leaderboard.objects.all()})


def SignupView(request):
    # accion post para submit de formulario de registo estudiantes
    if request.method == 'POST':
        form = StudentSignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
              #------------------------- Envio de correo  de confirmacion de cuenta-------
            salt = hashlib.sha1( (str(random.random())).encode('utf-8')).hexdigest()[:5]
            activation_key = hashlib.sha1((salt+user.email).encode('utf-8') ).hexdigest()
            key_expires = datetime.datetime.today() + datetime.timedelta(2)
            profileadministration = ProfileAdministration(user=user, activation_key=activation_key,   key_expires=key_expires)
            profileadministration.save()
            email_subject = 'Confirmación de cuenta'
            email_body = "Hola "+user.username+ ", Bienvenido a Fundamentos Play. Para poder jugar y acceder al sistema da clíck en este link en menos de 48 horas: \n" \
                         "http://200.126.12.59:8000/email_confirm/"+activation_key
            send_email(user.email, email_subject,  email_body)
            #------------------------------------------------------------------
            messages.success(request,
                             'Cuenta creada con éxito, podrás iniciar sesión cuando tu profesor apruebe la cuenta\n'
                             'y hayas confirmado tu email')
            return redirect('login')
        else:
            # Se envia el formulario y los profesores para llenar el combobox
            return render(request, 'registration/signup_form.html',
                          {"teachers": User.objects.filter(is_teacher=True), "form": form})
    # accion get para presentacion de formulario de registro estudiantes
    else:
        form = StudentSignUpForm()
        return render(request, 'registration/signup_form.html',
                      {"teachers": User.objects.filter(is_teacher=True), "form": form})



#view para la visualizacion de cualquier perfil  de usuario
@login_required
def view_profile(request,username):
    try:
        usuario = User.objects.get(username=username)
        fichas = FichaCompletacion.objects.filter(user = usuario)
    except User.DoesNotExist:
        raise Http404("Not Found")
    if( not request.user.is_admin):
        if( not usuario == request.user ):
            if (request.user.is_teacher):
                if (not usuario.profesor == request.user):  # comprobar  si el profesor puede ver su perfil
                    raise PermissionDenied
            else:
                raise PermissionDenied

    return render(request, 'FundamentosPlay/profile.html',
                  {"tipo":getTipo(request.user),"usuario": usuario, "fichas": fichas, "form": RolForm(usuario), 'passwordform':  SetPasswordForm(request.user)
                      , "counts": badges_counter(request.user)})


#reactivar usuario
@login_required
@has_permission
def edit_rol(request,  user_pk):
    if (request.method == 'POST'):
        form = RolForm(request.user,request.POST)
        if form.is_valid():
            user = User.objects.get(pk=user_pk)
            rol =form.cleaned_data['Rol']
            if(rol == "Ayudante"):
                user.is_student = False
                user.is_assistant = True
            elif (rol == "Estudiante"):
                user.is_student = True
                user.is_assistant = False
            user.save()
            messages.success(request, 'Rol cambiado exitosamente')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

# ambiar clave de usuario
@login_required
def edit_user_password(request, user_username):
    usuario = User.objects.get(username = user_username)
    if request.method == 'POST':
        form = SetPasswordForm(usuario, request.POST)
        if form.is_valid():
            user = form.save()
            messages.success(request, 'Contraseña actualizada')
            if(request.user.is_student):
                update_session_auth_hash(request, request.user)  # Important!
                redirect('login')
        else:
            for error in form.error_messages:
                messages.error(request, form.error_messages[error])
            if(request.user.is_student):
                redirect('login')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))



#borrar una usuario
@login_required
@has_permission
def user_delete(request,  user_pk):
    if (request.method == 'GET'):
        user = User.objects.get(pk=user_pk)
        user.is_active=False
        user.save()
        messages.success(request, 'Usuario deshabilidato exitosamente')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


#reactivar usuario
@login_required
@has_permission
def user_active(request,  user_pk):
    if (request.method == 'GET'):
        user = User.objects.get(pk=user_pk)
        user.is_active=True
        user.save()
        messages.success(request, 'Usuario Habilitado exitosamente')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def logout(request):
     request.user.is_logged= False
     request.user.save()
     return redirect('logout')


def email_confirm(request, activation_key):
    # Verifica que el usuario ya está logeado
    if request.user.is_authenticated:
        redirect('home')

    # Verifica que el token de activación sea válido y sino retorna un 404
    user_profile = get_object_or_404(ProfileAdministration, activation_key=activation_key)

    # verifica si el token de activación ha expirado y si es así renderiza el html de registro expirado
    if user_profile.key_expires < timezone.now():
        messages.info(request,
                             'Tiempo de verificación excedido')
        return redirect('login')
    # Si el token no ha expirado, se activa el usuario y se muestra el html de confirmación
    user_profile.is_confirmed = True
    user_profile.save()
    if(user_profile.user.is_active):
        messages.success(request,'Email verificado exitosamente, ahora podrás inicar sesión')
    else:
        messages.success(request,'Email verificado exitosamente, pero tu profesor aún no ha aprobado la cuenta')
    return redirect("login")




def descarga(request):
    return render(request, "FundamentosPlay/descarga.html", {'pagina':'Descarga'})


def leaderboard(request):
    return render(request, 'FundamentosPlay/leaderboard.html', {'pagina':'LeaderBoard','leaders':Leaderboard.objects.all()})

def sendMail(request):
    try:
        send_mail(
                request.POST["asunto"],
                "Correo de: "+ str(request.POST["from"])+ "\n\n" + str(request.POST["mensaje"]),
                'fundamentosplaygame@gmail.com',
                ['fundamentosplaygame@gmail.com'],
            )
    except:
        print("error al enviar mail")
    return redirect('home')





