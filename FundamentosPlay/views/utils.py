#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import hashlib
import random
from django.contrib.sessions.models import Session
from django.core.mail import send_mail
from django.template.backends import django
from django.utils import timezone


from ..models import Question, User, ProfileAdministration


#metodo para obtener los numeros de badges
def badges_counter(user):
    if(user.is_admin):  #si es admin no se le envia numeros en los badges
        enviadas = Question.objects.filter(state="Por Aprobar").count()
        aprobadas = Question.objects.filter(state="Aprobada").count()
        borrador = Question.objects.filter( state="Borrador").count()
        porCorregir = Question.objects.filter( state = "Por Corregir").count()
        return {"PA":enviadas , "AP":aprobadas, "BD":borrador, "PC":porCorregir}
    else:
        enviadas = Question.objects.filter(owner=user,state="Por Aprobar").count()
        aprobadas = Question.objects.filter(owner=user,state="Aprobada").count()
        borrador = Question.objects.filter(owner=user, state="Borrador").count()
        porCorregir = Question.objects.filter(owner=user, state = "Por Corregir").count()
        usuariosPA=   User.objects.filter(profesor=user , is_active=False).count()
        questionsPA = Question.objects.filter(revisor=user , state = "Por Aprobar").count()
        return {"PA":enviadas , "AP":aprobadas, "BD":borrador, "PC":porCorregir, "usersPA":usuariosPA, "questionsPA":questionsPA}


def send_email(para,asunto,mensaje):
    send_mail(
         asunto,
         mensaje,
        'fundamentosplaygame@gmail.com',
        [para],
        fail_silently=False,
    )




def getTipo(user):
    if (user.is_admin):
        tipo = "FundamentosPlay/administrator/administrator.html"
    elif (user.is_teacher):
        tipo = "FundamentosPlay/teachers/teacher.html"
    elif(user.is_student):
        tipo = "FundamentosPlay/students/student.html"
    else:
        tipo = "FundamentosPlay/assistants/assistant.html"
    return tipo



