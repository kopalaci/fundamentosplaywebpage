from rest_framework.generics import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import  Response

from ..models import FichaCompletacion, User, Leaderboard
from ..serializers import FichaSerializer, LeaderBoardSerializer


class Ficha(APIView):

    def get(self,request,username):
        if(request.method == 'GET'):
            fichas = []
            usuario = get_object_or_404(User, username=username)
            queryset = FichaCompletacion.objects.filter(user=usuario)
            for ficha in queryset:
                result = {"nivel": ficha.nivel, "principianteAcertadas": ficha.principianteAcertadas
                    , "principianteFalladas": ficha.principianteFalladas
                    , "intermedioAcertadas": ficha.intermedioAcertadas
                    , "intermedioFalladas": ficha.intermedioAcertadas
                    , "expertoAcertadas": ficha.expertoAcertadas
                    , "expertoFalladas": ficha.expertoAcertadas}
                fichas.append(result)
        serializador = FichaSerializer(fichas, many=True)
        return Response(serializador.data)

    def post(self):
        pass

class Nivel(APIView):

    def get(self,request,username):
        if(request.method == 'GET'):

            usuario = get_object_or_404(User, username=username)
            return Response(usuario.maximo_nivel)


class LeaderBoard(APIView):

    def get(self,request):
        querySet = Leaderboard.objects.all()

        serializador = LeaderBoardSerializer(querySet, many=True)
        return Response(serializador.data)

    def post(self):
        pass
