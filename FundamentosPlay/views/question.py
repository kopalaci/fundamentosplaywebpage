import datetime
from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from django.db import transaction
from django.forms import inlineformset_factory
from django import forms
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.views.generic import (ListView)
from FundamentosPlay.decorators import has_permission
from FundamentosPlay.forms import QuestionForm, QuestionToCorrectForm, BaseAnswerInlineFormSet
from FundamentosPlay.models import Question, Answer
from FundamentosPlay.views.utils import badges_counter, send_email, getTipo
from django.contrib import messages
from Proyecto import settings



#lista de preguntas
@method_decorator([login_required, has_permission], name='dispatch')
class QuestionListView(ListView):

    ordering = ('fecha', )
    context_object_name = 'questions'
    template_name = 'FundamentosPlay/question_change_list.html'


    def get_queryset(self):
        queryset = self.request.user.questions.filter(owner=self.request.user)
        return queryset



    def get_context_data(self, **kwargs):

        extra_context ={"tipo":getTipo(self.request.user),"titulo": "Mis Preguntas","counts": badges_counter(self.request.user)}
        context = super(self.__class__, self).get_context_data(**kwargs)
        for key, value in extra_context.items():
            if callable(value):
                context[key] = value()
            else:
                context[key] = value
        return context



#vista para crear  pregunta  sin respuestas
@login_required
@has_permission
def question_add(request):
    AnswerFormSet = inlineformset_factory(
        Question,  # parent model
        Answer,  # base model
        fields=('text', 'is_correct', 'retroalimentacion'),
        formset = BaseAnswerInlineFormSet,
        min_num = 2,
        validate_min = True,
        max_num = 4,
        validate_max = True,
        widgets = {
            'text': forms.Textarea(attrs={ 'cols': 80, 'rows': 5}),
            'retroalimentacion': forms.Textarea(attrs={'cols': 80, 'rows': 5 }),
        }

    )

    if request.method == 'POST':


        form = QuestionForm(request.user,None ,request.POST)
        answerform=None
        if form.is_valid()  :
            question = form.save(commit=False)
            question.owner = request.user
            question.state="Por Aprobar"
            if len(request.FILES) > 0 and request.FILES['Imagen']:
                myfile = request.FILES['Imagen']
                fs = FileSystemStorage()
                filename = fs.save(str(request.user.username)+str(datetime.date.today())+"."+myfile.name.split(".")[1],myfile)
                question.image = "http://"+request.get_host()+getattr(settings, "MEDIA_URL", None)+filename

            answerform = AnswerFormSet(request.POST , instance = question)
            if(answerform.is_valid()):
                with transaction.atomic():
                    form.save()
                    answerform.save()
                send_email(question.revisor.email, "Pregunta Recibida",
                           question.owner.username + " Le ha enviado pregunta")
                messages.success(request, 'Pregunta enviada exitosamente!')
                return redirect('questions:question_change_list')
    else:
        form = QuestionForm(request.user,None)
        answerform = AnswerFormSet()
    return render(request, 'FundamentosPlay/question_add_form.html', {'tipo':getTipo(request.user),'form': form ,'formset': answerform, 'counts': badges_counter(request.user)})



#vista para editar una pregunta
@login_required
@has_permission
def question_change(request,  question_pk):
    question = Question.objects.get(pk=question_pk)
    AnswerFormSet = inlineformset_factory(
        Question,  # parent model
        Answer,  # base model
        fields=('text', 'is_correct', 'retroalimentacion'),
        formset=BaseAnswerInlineFormSet,
        min_num=2,
        validate_min=True,
        max_num=4,
        validate_max=True,
        widgets={
            'text': forms.Textarea(attrs={'cols': 80, 'rows': 5}),
            'retroalimentacion': forms.Textarea(attrs={'cols': 80, 'rows': 5}),
        }
    )
    if request.method == 'POST':
        if(question.state =="Por Corregir"):
            form = QuestionToCorrectForm(request.user, request.POST, instance=question)
        else:
            form = QuestionForm(request.user ,question, request.POST, instance=question)
        formset = AnswerFormSet(request.POST ,instance=question)
        if form.is_valid():
            question = form.save(commit=False)

            question.state = "Por Aprobar"
            if len(request.FILES) > 0 and request.FILES['Imagen']:
                myfile = request.FILES['Imagen']
                fs = FileSystemStorage()
                filename = fs.save(str(request.user.username)+str(datetime.date.today())+"."+myfile.name.split(".")[1],myfile)
                question.image = "http://"+request.get_host()+getattr(settings, "MEDIA_URL", None)+filename
            if (formset.is_valid()):
                with transaction.atomic():
                    form.save()
                    formset.save()
                send_email(question.revisor.email, "Pregunta Recibida",
                           question.owner.username + " Le ha enviado pregunta para aprobar")
                messages.success(request, 'Pregunta enviada exitosamente!')
                return redirect('questions:question_change_list')


        else:
            return render(request, 'FundamentosPlay/question_change_form.html', {
                'tipo': getTipo(request.user),
                'question': question,
                'form': form,
                'formset': formset,
                'counts': badges_counter(request.user)})
    else:
        if (question.state == "Por Corregir"):
            form = QuestionToCorrectForm(request.user, instance=question)

        else:
            form = QuestionForm(request.user, question, instance=question)
        formset = AnswerFormSet(instance=question)
    return render(request, 'FundamentosPlay/question_change_form.html', {
        'tipo': getTipo(request.user),
        'question': question,
        'form': form,
        'formset': formset,
        'counts': badges_counter(request.user)})



#borrar una pregunta
@login_required
@has_permission
def question_delete(request,  question_pk):
    if (request.method == 'GET'):
        question = Question.objects.get(pk=question_pk)
        question.delete()
        messages.success(request, 'Pregunta Borrada exitosamente')
        return redirect('questions:question_change_list')



#guardar en borrador
@login_required
@has_permission()
def question_borrador(request ,question_pk):
    question = Question.objects.get(pk=question_pk)
    AnswerFormSet = inlineformset_factory(
        Question,  # parent model
        Answer,  # base model
        fields=('text', 'is_correct', 'retroalimentacion'),
        formset=BaseAnswerInlineFormSet,
        min_num=2,
        validate_min=True,
        max_num=4,
        validate_max=True,
        widgets={
            'text': forms.Textarea(attrs={'cols': 80, 'rows': 5}),
            'retroalimentacion': forms.Textarea(attrs={'cols': 80, 'rows': 5}),
        }
    )
    if request.method == 'POST':
        form = QuestionForm(request.user, question, request.POST, instance=question)
        formset = AnswerFormSet(request.POST, instance=question)
        if form.is_valid():
            question = form.save(commit=False)

            question.state = "Borrador"
            if len(request.FILES) > 0 and request.FILES['Imagen']:
                myfile = request.FILES['Imagen']
                fs = FileSystemStorage()
                filename = fs.save(str(request.user.username)+str(datetime.date.today())+"."+myfile.name.split(".")[1],myfile)
                question.image = "http://"+request.get_host()+getattr(settings, "MEDIA_URL", None)+filename
            if (formset.is_valid()):
                with transaction.atomic():
                    form.save()
                    formset.save()
                messages.success(request, 'Pregunta guardada en borradores')
                return redirect('questions:question_filter',"Borrador")
            else:
                return render(request, 'FundamentosPlay/question_change_form.html', {
                    'tipo': getTipo(request.user),
                    'question': question,
                    'form': form,
                    'formset': formset,
                    'counts': badges_counter(request.user)})

        else:
            return render(request, 'FundamentosPlay/question_change_form.html', {
                'tipo': getTipo(request.user),
                'question': question,
                'form': form,
                'formset': formset,
                'counts': badges_counter(request.user)})
    else:
        form = QuestionForm(request.user, question, instance=question)
        formset = AnswerFormSet(instance=question)
    return render(request, 'FundamentosPlay/question_change_form.html', {
        'tipo': getTipo(request.user),
        'question': question,
        'form': form,
        'formset': formset,
        'counts': badges_counter(request.user)})


#guardar en borrador
@login_required
@has_permission()
def question_borrador_nueva_pregunta(request):
    AnswerFormSet = inlineformset_factory(
        Question,  # parent model
        Answer,  # base model
        fields=('text', 'is_correct', 'retroalimentacion'),
        formset=BaseAnswerInlineFormSet,
        min_num=2,
        validate_min=True,
        max_num=4,
        validate_max=True,
        widgets={
            'text': forms.Textarea(attrs={'cols': 80, 'rows': 5}),
            'retroalimentacion': forms.Textarea(attrs={'cols': 80, 'rows': 5}),
        }

    )

    if request.method == 'POST':

        form = QuestionForm(request.user, None, request.POST)
        answerform = None
        if form.is_valid():
            question = form.save(commit=False)
            question.owner = request.user

            question.state = "Borrador"
            if len(request.FILES) > 0 and request.FILES['Imagen']:
                myfile = request.FILES['Imagen']
                fs = FileSystemStorage()
                filename = fs.save(str(request.user.username)+str(datetime.date.today())+"."+myfile.name.split(".")[1],myfile)
                question.image = "http://"+request.get_host()+getattr(settings, "MEDIA_URL", None)+filename
            answerform = AnswerFormSet(request.POST, instance=question)
            if (answerform.is_valid()):
                with transaction.atomic():
                    form.save()
                    answerform.save()
                return redirect('questions:question_change_list')
    else:
        form = QuestionForm(request.user, None)
        answerform = AnswerFormSet()
    return render(request, 'FundamentosPlay/question_add_form.html',
                  {'tipo': getTipo(request.user), 'form': form, 'formset': answerform,
                   'counts': badges_counter(request.user)})


#filtro de preguntas
@login_required
@has_permission
def question_filter(request,filter):

    if(request.method == 'GET'):
        questions = Question.objects.filter(owner=request.user, state= filter)
        return render(request, 'FundamentosPlay/question_change_list.html', {"tipo":getTipo(request.user),"titulo":filter,"questions": questions
                                                            , "counts":badges_counter(request.user)})

