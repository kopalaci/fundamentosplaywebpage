import datetime
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.files.storage import FileSystemStorage
from django.db import transaction
from django.forms import inlineformset_factory
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.decorators import method_decorator
from django.views.generic import (CreateView,ListView)

from Proyecto import settings
from ..views.utils import badges_counter, send_email
from ..decorators import teacher_required
from ..forms import BaseAnswerInlineFormSet, QuestionApproveForm
from ..models import Answer, Question, User
from django import forms

from django.contrib.auth import get_user_model
User = get_user_model()


#Lista de preguntas por aprobar
@method_decorator([login_required, teacher_required], name='dispatch')
class QuestionApproveList(ListView):

    ordering = ('text', )
    context_object_name = 'to_approve'
    template_name = 'FundamentosPlay/teachers/question_to_approve_list.html'

    def get_queryset(self):
        queryset = Question.objects.filter(revisor__username=self.request.user , state="Por Aprobar")
        return queryset

    def get_context_data(self, **kwargs):
        extra_context ={"counts": badges_counter(self.request.user)}
        context = super(self.__class__, self).get_context_data(**kwargs)
        for key, value in extra_context.items():
            if callable(value):
                context[key] = value()
            else:
                context[key] = value
        return context


#accion  de  aprobar pregunta
@login_required
@teacher_required
def question_aprove_view(request,  question_pk):
    question = get_object_or_404(Question, pk=question_pk)

    AnswerFormSet = inlineformset_factory(
        Question,  # parent model
        Answer,  # base model
        fields=('text', 'is_correct', 'retroalimentacion'),
        formset=BaseAnswerInlineFormSet,
        min_num=2,
        validate_min=True,
        max_num=4,
        validate_max=True,
        widgets={
            'text': forms.Textarea(attrs={'cols': 80, 'rows': 5}),
            'retroalimentacion': forms.Textarea(attrs={'cols': 80, 'rows': 5}),

        }

    )

    if request.method == 'POST':
        form = QuestionApproveForm(request.POST, instance=question)
        formset = AnswerFormSet(request.POST, instance=question)
        if form.is_valid():
            question = form.save(commit=False)
       #     estado = Estado.objects.get(nombre="AP")
        #    question.estado = estado
            question.state = "Aprobada"
            if len(request.FILES) > 0 and request.FILES['Imagen']:
                myfile = request.FILES['Imagen']
                fs = FileSystemStorage()
                filename = fs.save(str(request.user.username)+str(datetime.date.today())+"."+myfile.name.split(".")[1],myfile)
                question.image = "http://"+request.get_host()+getattr(settings, "MEDIA_URL", None)+filename
                question.save()
            if (formset.is_valid()):
                with transaction.atomic():
                    form.save()
                    formset.save()

            send_email(question.owner.email, "Pregunta Aprobada",
                       question.revisor.username + " ha aprobado su pregunta")
            messages.success(request, 'Pregunta Aprobada exitosamente!')
            return redirect('teachers:to_approve_list')
    else:
        if(question.revisor == request.user):
            form = QuestionApproveForm(instance=question )
            formset = AnswerFormSet(instance=question)
        else:
            raise PermissionDenied

    return render(request, 'FundamentosPlay/teachers/question_approve_form.html', {
        'question': question,
        'form': form,
        'formset': formset,
        'counts': badges_counter(request.user)
    })



#accion de  rechazar pregunta
@login_required
@teacher_required
def question_dimiss(request ,question_pk):
    question = get_object_or_404(Question, pk=question_pk)
    AnswerFormSet = inlineformset_factory(
        Question,  # parent model
        Answer,  # base model
        fields=('text', 'is_correct', 'retroalimentacion'),
        formset=BaseAnswerInlineFormSet,
        min_num=2,
        validate_min=True,
        max_num=4,
        validate_max=True,
        widgets={
            'text': forms.Textarea(attrs={'cols': 80, 'rows': 5}),
            'retroalimentacion': forms.Textarea(attrs={'cols': 80, 'rows': 5}),

        }

    )
    if request.method == 'POST':
        form = QuestionApproveForm(request.POST, instance=question)
        formset = AnswerFormSet(request.POST, instance=question)
        if form.is_valid():
            with transaction.atomic():
                question = form.save(commit=False)
                #estado = Estado.objects.get(nombre="PC")
                #question.estado = estado
                question.state = "Por Corregir"
                if len(request.FILES) > 0 and request.FILES['Imagen']:
                    myfile = request.FILES['Imagen']
                    fs = FileSystemStorage()
                    filename = fs.save(str(request.user.username)+str(datetime.date.today())+"."+myfile.name.split(".")[1],myfile)
                    question.image = "http://"+request.get_host()+getattr(settings, "MEDIA_URL", None)+filename
                question.observacion= request.POST.get('observacion',"No se adjuntó observación")
                if(question.observacion!=""):
                    question.save()
                    if (formset.is_valid()):
                        with transaction.atomic():
                            form.save()
                            formset.save()
                    send_email(question.owner.email, "Pregunta Rechazada",
                        question.revisor.username + " ha rechazado su pregunta y ha añadido la siguiente observación:\n"+
                        question.observacion)
                    messages.success(request, 'Observación enviada exitosamente!')
                else:
                    messages.error(request, 'Debe adjuntar una observación!')
                    return redirect('teachers:question_approve', question.pk)
            return redirect('teachers:to_approve_list')
    return redirect('teachers:to_approve_list')

#lista de estudiantes por aprobar
@method_decorator([login_required, teacher_required], name='dispatch')
class StudentToApproveViewList(ListView):
    context_object_name = 'students'
    template_name = 'FundamentosPlay/teachers/student_approve_list.html'
    def get_queryset(self):
        queryset = User.objects.filter(profesor=self.request.user, is_active = False)
        return queryset

    def get_context_data(self, **kwargs):
        extra_context ={"counts": badges_counter(self.request.user)}
        context = super(self.__class__, self).get_context_data(**kwargs)
        for key, value in extra_context.items():
            if callable(value):
                context[key] = value()
            else:
                context[key] = value
        return context


# accion de  aprobar estudiante
@login_required
@teacher_required
def student_approve(request, student_username):
    student = get_object_or_404(User, username=student_username)
    if request.method == 'GET':
        student.is_active = True
        student.save()
        send_email(student.email, "Cuenta Aprobada",
                   "Hola "+student.username+", "+request.user.username + " ha aprobado tu cuenta. Ya puedes empezar a jugar ! ")
        messages.success(request, 'Estudiante aprobado exitosamente!')
    return redirect('teachers:students_approve_list')

# accion de  rechazar estudiante
@login_required
@teacher_required
def student_dismiss(request, student_username):
    student = User.objects.get(username= student_username)

    if request.method == 'GET':
        student.delete()
        messages.success(request, 'Estudiante rechazado exitosamente!')
    return redirect('teachers:students_approve_list')

#Lista de mis estudiantes
@method_decorator([login_required, teacher_required], name='dispatch')
class MyStudentListView(ListView):
    context_object_name = 'students'
    template_name = 'FundamentosPlay/teachers/my_students.html'
    def get_queryset(self):
        queryset = User.objects.filter(profesor=self.request.user ,is_active=True)
        return queryset

    def get_context_data(self, **kwargs):
        extra_context ={"counts": badges_counter(self.request.user)}
        context = super(self.__class__, self).get_context_data(**kwargs)
        for key, value in extra_context.items():
            if callable(value):
                context[key] = value()
            else:
                context[key] = value
        return context

