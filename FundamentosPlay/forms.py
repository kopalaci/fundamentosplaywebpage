import hashlib

from crispy_forms.helper import FormHelper
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from django.forms.utils import ValidationError

from .models import (Question, User, Niveles, FichaCompletacion)






class BaseAnswerInlineFormSet(forms.BaseInlineFormSet):



    def add_fields(self, form, index):
        super(BaseAnswerInlineFormSet, self).add_fields(form, index)
        form.fields['text'].label = ''
        form.fields['text'].required = True
        form.fields['text'].widget.attrs['placeholder'] = "Respuesta"
        form.fields['is_correct'].label = ''
        form.fields['retroalimentacion'].label = ''
        form.fields['retroalimentacion'].widget.attrs['placeholder'] = "Retroalimentacion"

        instance = getattr(self, 'instance', None)
        print("hola")
        if(instance.state == "Aprobada"):
                form.fields['text'].disabled = True
                form.fields['is_correct'].disabled = True
                form.fields['retroalimentacion'].disabled = True


    def clean(self):
        super().clean()

        has_one_correct_answer = False
        for form in self.forms:
            if not form.cleaned_data.get('DELETE', False):
                if form.cleaned_data.get('is_correct', False):
                    has_one_correct_answer = True
                    break
        if not has_one_correct_answer:
            raise ValidationError('Debe marcar al menos una respuesta correcta.', code='no_correct_answer')





class StudentSignUpForm(UserCreationForm):
    Profesor = forms.ModelChoiceField(
        queryset=User.objects.filter(is_teacher=True),
        required=True
    )

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username', 'first_name', 'last_name', 'email',)

    def __init__(self, *args, **kwargs):
        super(StudentSignUpForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)

        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True
        self.fields['Profesor'].required = True

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_student = True
        user.is_active = False
        user.profesor =User.objects.get(username=self.cleaned_data.get('Profesor').username)
        user.save()

        #------------------------- Creacion de fichas de completacion-------
        for nivel in Niveles.objects.all():
            ficha = FichaCompletacion(user=user , nivel =nivel)
            ficha.save()
        #------------------------------------------------------------------
        hashid = hashlib.sha256(str(user.pk).encode('utf-8')).hexdigest()
        user.userHash = hashid
        user.save()


        return user



class QuestionForm(forms.ModelForm):
    Imagen = forms.FileField()

    class Meta:
        model = Question
        fields = ('text', 'level','Dificultad','revisor','tiempo',)
        widgets = {
            'text': forms.Textarea(attrs={'cols': 80, 'rows': 20}),
        }


    def __init__(self,user,question, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.fields['revisor'].queryset = User.objects.filter(is_teacher = True).exclude(username = user.username)
        self.fields['level'].queryset = Niveles.objects.all()
        self.fields['tiempo'] = forms.IntegerField(min_value=5)
        self.fields['Imagen'].required = False
        self.fields['revisor'].required = True


        if(question != None):
            if question.state == "Aprobada":

                self.fields['text'].disabled = True
                self.fields['tiempo'].disabled = True
                self.fields['level'].disabled = True
                self.fields['Dificultad'].disabled = True
                self.fields['revisor'].disabled = True
            if question.state == "Por Aprobar":
                self.fields['revisor'].disabled = True




class QuestionToCorrectForm(forms.ModelForm):
    Imagen = forms.FileField()
    class Meta:
        model = Question
        fields = ('text', 'level','Dificultad','revisor','tiempo','observacion',)
        widgets = {
            'text': forms.Textarea(attrs={'cols': 80, 'rows': 20}),
            'observacion': forms.Textarea(attrs={'cols': 80, 'rows': 5}),

        }

    def __init__(self,user, *args, **kwargs):
        super(QuestionToCorrectForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.fields['revisor'].queryset = User.objects.filter(is_teacher = True).exclude(username = user.username)
        self.fields['revisor'].widget.attrs['readonly'] = True
        self.fields['observacion'].widget.attrs['readonly'] = True
        self.fields['revisor'].disabled= True
        self.fields['revisor'].required = True
        self.fields['Imagen'].required = False


class QuestionApproveForm(forms.ModelForm):
    Imagen = forms.FileField()
    class Meta:
        model = Question
        fields = ('text', 'level','Dificultad','tiempo','observacion','owner')
        widgets = {
            'text': forms.Textarea(attrs={'cols': 80, 'rows': 20}),
            'observacion': forms.Textarea(attrs={'cols': 80, 'rows': 5}),

        }


    def __init__(self, *args, **kwargs):
        super(QuestionApproveForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.fields['level'].queryset = Niveles.objects.all()
        self.fields['tiempo'] = forms.IntegerField(min_value=5)
        self.fields['Imagen'].required = False
        self.fields['owner'].disabled = True









##################################################################################################################################################

class TeacherSignUpForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

    def __init__(self, *args, **kwargs):
        super(TeacherSignUpForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)

        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_teacher = True
        user.is_active = True
        user.save()
        hashid = hashlib.sha256(str(user.pk).encode('utf-8')).hexdigest()
        user.userHash = hashid
        user.save()

        for nivel in Niveles.objects.all():
            ficha = FichaCompletacion(user=user , nivel =nivel)
            ficha.save()
        return user


class BaseAnswerInlineFormSet(forms.BaseInlineFormSet):

    def add_fields(self, form, index):
        super(BaseAnswerInlineFormSet, self).add_fields(form, index)
        form.fields['text'].label = ''
        form.fields['text'].required = True
        form.fields['text'].widget.attrs['placeholder'] = "Respuesta"
        form.fields['is_correct'].label = ''
        form.fields['retroalimentacion'].label = ''
        form.fields['retroalimentacion'].widget.attrs['placeholder'] = "Retroalimentacion"

    def clean(self):
        super().clean()
        has_one_correct_answer = False
        for form in self.forms:
            if not form.cleaned_data.get('DELETE', False):
                if form.cleaned_data.get('is_correct', False):
                    has_one_correct_answer = True
                    break
        if not has_one_correct_answer:
            raise ValidationError('Debe marcar al menos una respuesta correcta.', code='no_correct_answer')



class QuestionAdminForm(forms.ModelForm):
    Imagen = forms.FileField()

    class Meta:
        model = Question
        fields = ('owner','text', 'level','Dificultad','tiempo','revisor')
        widgets = {
            'text': forms.Textarea(attrs={'cols': 80, 'rows': 20}),
        }


    def __init__(self,user,question, *args, **kwargs):
        super(QuestionAdminForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.fields['level'].queryset = Niveles.objects.all()
        self.fields['tiempo'] = forms.IntegerField(min_value=5)
        self.fields['Imagen'].required = False




class AssistantSignUpForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username', 'first_name', 'last_name', 'email',)
    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_assistant = True
        user.is_active = True

        if commit:
            user.save()
            hashid = hashlib.sha256(str(user.pk).encode('utf-8')).hexdigest()
            user.userHash = hashid
            user.save()
            for nivel in Niveles.objects.all():
                ficha = FichaCompletacion(user=user , nivel =nivel)
                ficha.save()
        return user

class RolForm(forms.Form):
    ESTUDIANTE = 'Estudiante'
    AYUDANTE = 'Ayudante'
    ROLES= (
        (ESTUDIANTE, u"Estudiante"),
        (AYUDANTE, u"Ayudante")
    )
    Rol = forms.ChoiceField(choices=ROLES,  label='')

    def __init__(self,user,*args, **kwargs):
        super(RolForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        if(user.is_assistant):
            self.fields['Rol'].initial = "Ayudante"



