$(window).on('load',function(){

    //habilitar el uso de tab como identacion en texareas
    var textareas = document.getElementsByTagName('textarea');
    var count = textareas.length;
    for(var i=0;i<count;i++){
        textareas[i].onkeydown = function(e){
            if(e.keyCode==9 || e.which==9){
                e.preventDefault();
                var s = this.selectionStart;
                this.value = this.value.substring(0,this.selectionStart) + "\t" + this.value.substring(this.selectionEnd);
                this.selectionEnd = s + "\t".length;
            }
        }
    }





    //anadir  respuestas fields

	$('#add_button').on('click',function(){
	        console.log("click")
            $('.hide').removeClass("hide");
            $('#add_button').css('display','none');
            $("#id_answers-2-text").prop('required', false);
            $("#id_answers-2-retroalimentacion").prop('required', false);
            $("#id_answers-3-text").prop('required', false);
            $("#id_answers-3-retroalimentacion").prop('required', false);


//
    });


    $(document).on('click','.checkboxinput',function(){
        var index = this.name.split("-")[1];
        var id = "id_answers-"+index+"-is_correct";
         var checkbox = document.getElementById(id);
        if( $("input[id="+id+"]").is(":checked")) {
            $("#id_answers-"+index+"-retroalimentacion").val('');
            $("#id_answers-"+index+"-retroalimentacion").prop('disabled', true);
        }
        else{
            $("#id_answers-"+index+"-retroalimentacion").prop('disabled', false);
        }

    });

 // borrar respuesta
    $('.deletebutton').on('click',function(){
         var actual_index = this.title;
         $("#id_answers-"+(actual_index-1)+"-DELETE").prop('checked', true);
        var row_id = ".row-"+(actual_index);
        console.log(row_id);
        $(row_id).addClass("hide");
        $('#add_button').css('display','block');
        $("#id_answers-"+actual_index+"-text").val('');
        $("#id_answers-"+actual_index+"-retroalimentacion").val('');
        $("#id_answers-"+actual_index+"-is_correct").prop('checked', false);
    });




    // guardar en borrador
    $('#save').on('click',function(){
        console.log("save")
	    var LOCAL_URL = "/questions/question/save/"+this.name+"/";
	    $("#form").attr("action",LOCAL_URL);
	    $('#form').submit();


    });

    // guardar en borrador nueva pregunta
    $('#save_nueva').on('click',function(){
        console.log("save")
	    var LOCAL_URL = "/questions/question/save/";
	    $("#form").attr("action",LOCAL_URL);
	    $('#form').submit();


    });









})