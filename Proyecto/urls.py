from django.urls import include, path
from FundamentosPlay.views import home
urlpatterns = [
    path('', include('FundamentosPlay.urls')),
    path('accounts/', include('django.contrib.auth.urls')),  # login  es manejado por librerias de django, configuracion de paginas en settings
    path('accounts/signup/', home.SignupView, name='signup'),
]
